import 'dart:convert';
import 'dart:io';

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart';
import 'package:shelf_router/shelf_router.dart';
import 'cancel_trip_resources/cancel_trip_response.dart';
import 'cancel_trip_resources/passenger_trip.dart';
import 'cancel_trip_algorithm.dart';
import 'matching_algorithm.dart';
import 'matching_resources/osrm_route_resource.dart';
import 'matching_resources/matching_response.dart';

// Configure routes.

final _router = Router()
  ..get('/', _rootHandler)
  ..post('/trip/create', _createTripHandler)
  ..post('/trip/cancel', _cancelTripHandler);

Future<Response> _rootHandler(Request req) async {
  return Response.ok('Motis Server for Driver Matching');
}

Future<Response> _createTripHandler(Request request) async {
  String requestString = await request.readAsString();
  dynamic rawBody = jsonDecode(requestString);
  TripInformation tripInformation = TripInformation.fromJson(rawBody);

  MatchingAlgorithm matchingService = MatchingAlgorithm(tripInformation);

  MatchingResponse matchingResponse = await matchingService.matchTrip();
  return Response.ok(jsonEncode(matchingResponse));
}

Future<Response> _cancelTripHandler(Request request) async {
  String requestString = await request.readAsString();
  dynamic rawBody = jsonDecode(requestString);
  PassengerTrip passengerTripToCancel = PassengerTrip.fromJson(rawBody);

  CancelTripService cancelService = CancelTripService();

  CancelTripResponse cancelTripResponse =
      await cancelService.cancelTrip(passengerTripToCancel);

  return Response.ok(jsonEncode(cancelTripResponse));
}

void main(List<String> args) async {
  // Use any available host or container IP (usually `0.0.0.0`).
  final ip = InternetAddress.anyIPv4;

  // Configure a pipeline that logs requests.
  final handler = Pipeline().addMiddleware(logRequests()).addHandler(_router);

  // For running in containers, we respect the PORT environment variable.
  final port = int.parse(Platform.environment['PORT'] ?? '8000');
  final server = await serve(handler, ip, port);
  print('Server listening on port ${server.port}');
}
