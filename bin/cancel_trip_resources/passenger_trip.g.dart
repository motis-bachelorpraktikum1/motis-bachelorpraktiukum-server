// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'passenger_trip.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PassengerTrip _$PassengerTripFromJson(Map<String, dynamic> json) =>
    PassengerTrip(
      tripId: json['tripId'] as String,
      requestedAt: DateTime.parse(json['requestedAt'] as String),
      driverId: json['driverId'] as String,
      passengerId: json['passengerId'] as String,
      cancelled: json['cancelled'] as bool,
      scheduledOn: DateTime.parse(json['scheduledOn'] as String),
      duration: json['duration'] as int,
      startLocation: json['startLocation'] as String,
      endLocation: json['endLocation'] as String,
      startPointDisplayName: json['startPointDisplayName'] as String,
      destinationDisplayName: json['destinationDisplayName'] as String,
      isDeparturePicked: json['isDeparturePicked'] as bool,
      rating: json['rating'] as int,
    );

Map<String, dynamic> _$PassengerTripToJson(PassengerTrip instance) =>
    <String, dynamic>{
      'tripId': instance.tripId,
      'requestedAt': instance.requestedAt.toIso8601String(),
      'driverId': instance.driverId,
      'passengerId': instance.passengerId,
      'cancelled': instance.cancelled,
      'scheduledOn': instance.scheduledOn.toIso8601String(),
      'duration': instance.duration,
      'startLocation': instance.startLocation,
      'endLocation': instance.endLocation,
      'startPointDisplayName': instance.startPointDisplayName,
      'destinationDisplayName': instance.destinationDisplayName,
      'isDeparturePicked': instance.isDeparturePicked,
      'rating': instance.rating,
    };
