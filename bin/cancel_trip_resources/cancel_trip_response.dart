import 'package:json_annotation/json_annotation.dart';

import '../matching_state.dart';

part 'cancel_trip_response.g.dart';

@JsonSerializable()
class CancelTripResponse {
  MatchingState status;
  String? message;

  CancelTripResponse({required this.status, this.message});

  factory CancelTripResponse.fromJson(Map<String, dynamic> json) =>
      _$CancelTripResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CancelTripResponseToJson(this);
}
