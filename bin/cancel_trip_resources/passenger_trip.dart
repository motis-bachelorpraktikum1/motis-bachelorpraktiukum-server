import 'package:json_annotation/json_annotation.dart';

part 'passenger_trip.g.dart';

@JsonSerializable(explicitToJson: true)
class PassengerTrip {
  String tripId;
  DateTime requestedAt;
  String driverId;
  String passengerId;
  bool cancelled;
  DateTime scheduledOn;
  int duration;
  String startLocation;
  String endLocation;
  String startPointDisplayName;
  String destinationDisplayName;
  bool isDeparturePicked;
  int rating;

  PassengerTrip({
    required this.tripId,
    required this.requestedAt,
    required this.driverId,
    required this.passengerId,
    required this.cancelled,
    required this.scheduledOn,
    required this.duration,
    required this.startLocation,
    required this.endLocation,
    required this.startPointDisplayName,
    required this.destinationDisplayName,
    required this.isDeparturePicked,
    required this.rating,
  });

  factory PassengerTrip.fromJson(Map<String, dynamic> json) =>
      _$PassengerTripFromJson(json);

  Map<String, dynamic> toJson() => _$PassengerTripToJson(this);
}
