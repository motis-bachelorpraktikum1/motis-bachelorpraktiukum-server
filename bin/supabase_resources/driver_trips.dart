import 'dart:convert';

import 'package:uuid/uuid.dart';

import '../matching_resources/predictions_resource.dart';
import 'constants.dart';
import '../supabase_resources/supabase_tables.dart' as supabase_tables;

class DriverTrip {
  static String SbVar_driverTripId = 'id';
  String? driverTripId;
  static String SbVar_driverId = 'driver_id';
  String? driverId;
  static String SbVar_startTime = 'start_time';
  DateTime? startTime;
  static String SbVar_endTime = 'end_time';
  DateTime? endTime;
  static String SbVar_startLocation = 'start_location';
  PositionModel? startLocation;
  static String SbVar_endLocation = 'end_location';
  PositionModel? endLocation;
  static String SbVar_duration = 'duration';
  num? duration;
  static String SbVar_passengers = 'passengers';
  List<String>? passengers;
  static String SbVar_availableSeats = 'remaining_seats';
  int? availableSeats;
  static String SbVar_startAdress = 'start_adress';
  String? startAdress;
  static String SbVar_endAdress = 'end_adress';
  String? endAdress;

  static DriverTrip parseDriverOnDuty(Map<dynamic, dynamic> driverTripMap) {
    DriverTrip driverTrip = new DriverTrip();

    driverTrip.driverTripId = driverTripMap[SbVar_driverTripId] as String;

    driverTrip.driverId = driverTripMap[SbVar_driverId] as String;

    driverTrip.startTime =
        DateTime.parse(driverTripMap[SbVar_startTime] as String);

    driverTrip.endTime = DateTime.parse(driverTripMap[SbVar_endTime] as String);

    driverTrip.startLocation =
        _resolveCoordinateString(driverTripMap[SbVar_startLocation] as String);

    driverTrip.endLocation =
        _resolveCoordinateString(driverTripMap[SbVar_endLocation] as String);

    final String passengerListJson = driverTripMap[SbVar_passengers] as String;
    final decodedList = jsonDecode(passengerListJson) as List<dynamic>;
    driverTrip.passengers = decodedList.map((e) => e as String).toList();

    driverTrip.availableSeats = driverTripMap[SbVar_availableSeats] as int;

    driverTrip.duration = driverTripMap[SbVar_duration] as num;

    driverTrip.startAdress = driverTripMap[SbVar_startAdress] as String;
    driverTrip.endAdress = driverTripMap[SbVar_endAdress] as String;

    return driverTrip;
  }

  static PositionModel _resolveCoordinateString(String coordinateString) {
    final List<String> parts = coordinateString.split(', ');
    final PositionModel coordinates =
        PositionModel.fromJson({'lat': parts[0], 'lng': parts[1]});
    return coordinates;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.driverTripsTableId).insert({
      SbVar_driverTripId: driverTripId ?? const Uuid().v4(),
      SbVar_driverId: driverId,
      SbVar_passengers: jsonEncode(passengers),
      SbVar_startTime: startTime!.toString(),
      SbVar_endTime: endTime!.toString(),
      SbVar_startLocation: startLocation.toString(),
      SbVar_endLocation: endLocation.toString(),
      SbVar_duration: duration,
      SbVar_availableSeats: availableSeats,
      SbVar_startAdress: startAdress,
      SbVar_endAdress: endAdress,
    });
  }
}
