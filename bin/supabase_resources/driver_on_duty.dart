import 'constants.dart';
import 'location_model.dart';
import 'route-model.dart';
import '../supabase_resources/supabase_tables.dart' as supabase_tables;

class DriverOnDuty {
  static String SbVar_userId = 'id';
  String? userId;
  static String SbVar_route = 'route';
  RouteModel? routeModel;
  static String SbVar_driverLocation = 'current_location';
  CurrentPosition? locationModel;
  static String SbVar_availableSeats = 'available_seats';
  int? availableSeats;

  static DriverOnDuty parseDriverOnDuty(Map<dynamic, dynamic> driverOnDutyMap) {
    DriverOnDuty driverOnDuty = new DriverOnDuty();

    driverOnDuty.userId = driverOnDutyMap[SbVar_userId] as String;

    if (driverOnDutyMap[SbVar_route] == null) {
      driverOnDuty.routeModel = RouteModel(trips: []);
    } else {
      driverOnDuty.routeModel = RouteModel.fromJson(
        driverOnDutyMap[SbVar_route] as Map<String, dynamic>,
      );
    }

    if (driverOnDutyMap[SbVar_driverLocation] == null) {
      driverOnDuty.locationModel = null;
    } else {
      driverOnDuty.locationModel = CurrentPosition.fromJson(
        driverOnDutyMap[SbVar_driverLocation] as Map<String, dynamic>,
      );
    }

    driverOnDuty.availableSeats = driverOnDutyMap[SbVar_availableSeats] as int;
    return driverOnDuty;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.driverOnDutyTableId).upsert({
      SbVar_userId: userId,
      SbVar_route: routeModel!.toJson(),
      SbVar_driverLocation: locationModel!.toJson(),
      SbVar_availableSeats: availableSeats,
    }).eq(SbVar_userId, userId);
  }
}
