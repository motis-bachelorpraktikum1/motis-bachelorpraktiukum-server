import 'package:latlong2/latlong.dart';
import 'supabase_tables.dart' as supabase_tables;

import 'constants.dart';

class Trip {
  static String SbVar_tripId = 'id';
  String? tripId;
  static String SbVar_requestedAt = 'requested_at';
  DateTime? requestedAt;
  static String SbVar_driverId = 'driver';
  String? driverId;
  static String SbVar_passengerId = 'passenger';
  String? passengerId;
  static String SbVar_cancelled = 'cancelled';
  bool? cancelled;
  static String SbVar_scheduledOn = 'scheduled_on';
  DateTime? scheduledOn;
  static String SbVar_duration = 'duration';
  int? duration;
  static String SbVar_startLocation = 'start_location';
  String? startLocation;
  static String SbVar_endLocation = 'end_location';
  String? endLocation;

  static String SbVar_startPointDisplayName = 'start_point_display_name';
  String? startPointDisplayName;
  static String SbVar_destinationDisplayName = 'destination_display_name';
  String? destinationDisplayName;

  static String SbVar_departurePicked = 'departure_picked';
  bool? isDeparturePicked;

  static Trip parseTripModel(Map<dynamic, dynamic> tripMap) {
    Trip trip = new Trip();
    trip.tripId = tripMap[SbVar_tripId] as String;
    trip.requestedAt = DateTime.parse(tripMap[SbVar_requestedAt] as String);
    trip.driverId = (tripMap[SbVar_driverId] ?? '') as String;
    trip.passengerId = tripMap[SbVar_passengerId] as String;
    trip.cancelled = tripMap[SbVar_cancelled] as bool;
    trip.duration = tripMap[SbVar_duration] as int;
    trip.scheduledOn = DateTime.parse(tripMap[SbVar_scheduledOn] as String);
    trip.startLocation = tripMap[SbVar_startLocation] as String;
    trip.endLocation = tripMap[SbVar_endLocation] as String;
    trip.startPointDisplayName = tripMap[SbVar_startPointDisplayName] as String;
    trip.destinationDisplayName =
        tripMap[SbVar_destinationDisplayName] as String;
    trip.isDeparturePicked = tripMap[SbVar_departurePicked] as bool;
    return trip;
  }

  static LatLng resolveCoordinateString(String coordinateString) {
    final int idx = coordinateString.indexOf(", ");
    final List<double> parts = [
      double.parse(coordinateString.substring(0, idx).trim()),
      double.parse(coordinateString.substring(idx + 1).trim()),
    ];
    final LatLng coordinates = LatLng(parts[0], parts[1]);
    return coordinates;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.tripTableId).upsert({
      SbVar_tripId: tripId,
      SbVar_driverId: driverId,
      SbVar_requestedAt: requestedAt.toString(),
      SbVar_passengerId: passengerId,
      SbVar_cancelled: cancelled.toString(),
      SbVar_duration: duration,
      SbVar_scheduledOn: scheduledOn!.toString(),
      SbVar_startLocation: startLocation.toString(),
      SbVar_endLocation: endLocation.toString(),
      SbVar_startPointDisplayName: startPointDisplayName,
      SbVar_destinationDisplayName: destinationDisplayName,
      SbVar_departurePicked: isDeparturePicked.toString(),
    }).eq(SbVar_tripId, tripId);
  }
}
