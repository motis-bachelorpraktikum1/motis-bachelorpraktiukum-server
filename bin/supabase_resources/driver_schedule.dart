import 'constants.dart';
import '../supabase_resources/supabase_tables.dart' as supabase_tables;

class DriverSchedule {
  static String SbVar_driverId = 'driver_id';
  String? driverId;

  static String SbVar_startTime = 'start_time';
  DateTime? startTime;

  static String SbVar_endTime = 'end_time';
  DateTime? endTime;

  static DriverSchedule parseDriverOnDuty(
      Map<dynamic, dynamic> driverScheduleMap) {
    DriverSchedule driverSchedule = new DriverSchedule();

    driverSchedule.driverId = driverScheduleMap[SbVar_driverId] as String;
    driverSchedule.startTime =
        DateTime.parse(driverScheduleMap[SbVar_startTime] as String);
    driverSchedule.endTime =
        DateTime.parse(driverScheduleMap[SbVar_endTime] as String);

    return driverSchedule;
  }

  Future<void> saveToSupabase() async {
    await supabase
        .from(supabase_tables.driverScheduleTableId)
        .update({SbVar_startTime: startTime, SbVar_endTime: endTime}).eq(
            SbVar_driverId, driverId);
  }
}
