import 'location_model.dart';

class RouteModel {
  List<DriverTripJson> trips;

  RouteModel({required this.trips});

  factory RouteModel.fromJson(Map<String, dynamic> json) {
    List test = json['trips'] as List;
    if ((json['trips'] as List).isEmpty) {
      return RouteModel(trips: []);
    } else {
      return RouteModel(
        trips: (json['trips'] as List<dynamic>)
            .map((e) => DriverTripJson.fromJson(e as Map<String, dynamic>))
            .toList(),
      );
    }
  }

  Map toJson() => {
        'trips': trips.map((e) => e.toJson()).toList(),
      };
}

class DriverTripJson {
  CurrentPosition startPoint;
  CurrentPosition destinationPoint;
  String startAdress;
  String destinationAdress;
  DateTime date;
  String passengerId;

  DriverTripJson({
    required this.startPoint,
    required this.destinationPoint,
    required this.startAdress,
    required this.destinationAdress,
    required this.date,
    required this.passengerId,
  });

  Map toJson() => {
        'start-point': startPoint.toJson(),
        'destination-point': destinationPoint.toJson(),
        'start-adress': startAdress,
        'destination-adress': destinationAdress,
        'date': date.toIso8601String(),
        'passenger': passengerId,
      };

  factory DriverTripJson.fromJson(Map<String, dynamic> json) {
    return DriverTripJson(
      startPoint:
          CurrentPosition.fromJson(json['start-point'] as Map<String, dynamic>),
      destinationPoint: CurrentPosition.fromJson(
          json['destination-point'] as Map<String, dynamic>),
      startAdress: json['start-adress'] as String,
      destinationAdress: json['destination-adress'] as String,
      date: DateTime.parse(json['date'] as String),
      passengerId: json['passenger'] as String,
    );
  }
}
