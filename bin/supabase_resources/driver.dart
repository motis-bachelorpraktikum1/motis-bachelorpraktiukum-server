import 'constants.dart';
import '../supabase_resources/supabase_tables.dart' as supabase_tables;

class Driver {
  static String SbVar_userId = 'id';
  String? userId;
  static String SbVar_currentVehicleId = 'current_vehicle';
  String? currentVehicleId;
  static String SbVar_areaOfOperationPolygons = 'area_of_operation_polygons';
  dynamic areaOfOperationPolygons;
  static String SbVar_fleetAssignedId = 'fleet_assigned';
  String? fleetId;

  static Driver parseDriverModel(Map<dynamic, dynamic> driverMap) {
    Driver driver = new Driver();
    driver.userId = driverMap[SbVar_userId] as String;
    driver.currentVehicleId =
        (driverMap[SbVar_currentVehicleId] ?? '') as String;
    driver.areaOfOperationPolygons =
        (driverMap[SbVar_areaOfOperationPolygons] ?? []) as dynamic;
    driver.fleetId = (driverMap[SbVar_fleetAssignedId] ?? '') as String;
    return driver;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.driverTableId).upsert({
      SbVar_userId: userId,
      SbVar_currentVehicleId:
          currentVehicleId!.isEmpty ? null : currentVehicleId,
      SbVar_fleetAssignedId: fleetId!.isEmpty ? null : fleetId
    }).eq(SbVar_userId, userId);
  }
}
