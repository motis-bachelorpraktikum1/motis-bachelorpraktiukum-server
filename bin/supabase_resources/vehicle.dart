import 'constants.dart';
import '../supabase_resources/supabase_tables.dart' as supabase_tables;

class Vehicle {
  static String SbVar_vehicleId = 'id';
  String? vehicleId;
  static String SbVar_licensePlate = 'license_plate';
  String? licensePlate;
  static String SbVar_fleetId = 'fleet_id';
  String? fleetId;
  static String SbVar_vehicleName = 'vehicle_name';
  String? vehicleName;
  static String SbVar_numberOfSeats = 'number_of_seats';
  int? numberOfSeats;
  static String SbVar_numberOfDisabilityFriendlySeats =
      'number_of_disability_friendly_seats';
  int? numberOfDisabilityFriendlySeats;
  static String SbVar_driverId = 'driver_id';
  String? driverID;

  static Vehicle parseVehicleModel(Map<dynamic, dynamic> vehicleMap) {
    Vehicle vehicle = Vehicle();
    vehicle.vehicleId = vehicleMap[SbVar_vehicleId] as String;
    vehicle.licensePlate = vehicleMap[SbVar_licensePlate] as String;
    vehicle.fleetId = (vehicleMap[SbVar_fleetId] ?? '') as String;
    vehicle.vehicleName = vehicleMap[SbVar_vehicleName] as String;
    vehicle.numberOfSeats = vehicleMap[SbVar_numberOfSeats] as int;
    vehicle.numberOfDisabilityFriendlySeats =
        vehicleMap[SbVar_numberOfDisabilityFriendlySeats] as int;
    vehicle.driverID = (vehicleMap[SbVar_driverId] ?? '') as String;
    return vehicle;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.vehicleTableId).upsert({
      SbVar_vehicleId: vehicleId,
      SbVar_licensePlate: licensePlate,
      SbVar_fleetId: fleetId!.isEmpty ? null : fleetId,
      SbVar_vehicleName: vehicleName,
      SbVar_numberOfSeats: numberOfSeats,
      SbVar_numberOfDisabilityFriendlySeats: numberOfDisabilityFriendlySeats,
      SbVar_driverId: driverID!.isEmpty ? null : driverID,
    }).eq(SbVar_vehicleId, vehicleId);
  }
}
