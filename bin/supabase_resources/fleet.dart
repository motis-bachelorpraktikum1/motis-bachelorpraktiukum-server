import 'constants.dart';
import '../supabase_resources/supabase_tables.dart' as supabase_tables;

class Fleet {
  static String SbVar_fleetId = 'id';
  String? fleetId;
  static String SbVar_fleetName = 'name';
  String? fleetName;

  static Fleet parseFleetMap(Map<dynamic, dynamic> fleetMap) {
    Fleet fleet = new Fleet();
    fleet.fleetId = fleetMap[SbVar_fleetId] as String;
    fleet.fleetName = fleetMap[SbVar_fleetName] as String;
    return fleet;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.fleetTableId).upsert({
      SbVar_fleetId: fleetId,
      SbVar_fleetName: fleetName,
    }).eq(SbVar_fleetId, fleetId);
  }
}
