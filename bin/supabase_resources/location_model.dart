import 'package:latlong2/latlong.dart';

class CurrentPosition {
  LatLng position;

  CurrentPosition({required this.position});

  factory CurrentPosition.fromJson(Map<String, dynamic> json) {
    return CurrentPosition(
      position: LatLng(
        json['latitude'] as double,
        json['longitude'] as double,
      ),
    );
  }

  Map toJson() => {
        'longitude': position.longitude,
        'latitude': position.latitude,
      };
}
