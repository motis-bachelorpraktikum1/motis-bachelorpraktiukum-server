import 'constants.dart';
import '../supabase_resources/supabase_tables.dart' as supabase_tables;

class Passenger {
  static String SbVar_userId = 'id';
  String? userId;
  static String SbVar_disabilityFriendlyRequired =
      'disability-friendly-required';
  bool? disabilityFriendlyRequired;

  static Passenger parsePassenger(Map<dynamic, dynamic> passengerMap) {
    Passenger passenger = Passenger();
    passenger.userId = passengerMap[SbVar_userId] as String;
    passenger.disabilityFriendlyRequired =
        passengerMap[SbVar_disabilityFriendlyRequired] as bool;
    return passenger;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.passengerTableId).upsert({
      SbVar_userId: userId,
      SbVar_disabilityFriendlyRequired: disabilityFriendlyRequired,
    }).eq(SbVar_userId, userId);
  }
}
