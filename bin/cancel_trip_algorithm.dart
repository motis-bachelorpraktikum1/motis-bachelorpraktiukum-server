import 'dart:convert';

import 'cancel_trip_resources/cancel_trip_response.dart';
import 'cancel_trip_resources/passenger_trip.dart';
import 'matching_resources/osrm_route_resource.dart';
import 'matching_state.dart';
import 'supabase_resources/constants.dart';
import 'supabase_resources/driver_trips.dart';

class CancelTripService {
  Future<CancelTripResponse> cancelTrip(PassengerTrip trip) async {
    //Get first driver_trip
    List firstDriverTrip = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', trip.driverId)
        .eq('start_time', trip.scheduledOn) as List;

    DriverTrip? currendDriverTrip = DriverTrip.parseDriverOnDuty(
        firstDriverTrip[0] as Map<String, dynamic>);

    //get driver_trip before first
    List beforeDriverTripList = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', trip.driverId)
        .eq('end_time', trip.scheduledOn)
        .limit(1) as List;

    //Delete first driver trip, if there is a trip before bridge them, just
    //delete passenger from first Trip if other Passenger uses stop
    if (beforeDriverTripList.isNotEmpty) {
      DriverTrip beforeDriverTrip = DriverTrip.parseDriverOnDuty(
          beforeDriverTripList[0] as Map<String, dynamic>);

      if (beforeDriverTrip.availableSeats ==
              (currendDriverTrip.availableSeats! - 1) &&
          currendDriverTrip.passengers!.length != 1) {
        await removeTrip(beforeDriverTrip);
        await removeTrip(currendDriverTrip);
        await createDriverTrip(beforeDriverTrip, currendDriverTrip);
      } else {
        await removePassengerFromTrip(currendDriverTrip, trip);
      }
    } else {
      removeTrip(currendDriverTrip);
    }

    //Delete passenger from Trips till end
    currendDriverTrip = await nextDriverTrip(currendDriverTrip);
    while (currendDriverTrip != null) {
      List<String> passengerList = currendDriverTrip.passengers!;
      if (passengerList.contains(trip.passengerId) &&
          passengerList.length == 1) {
        removeTrip(currendDriverTrip);
      } else if (passengerList.contains(trip.passengerId)) {
        removePassengerFromTrip(currendDriverTrip, trip);
      } else if (!passengerList.contains(trip.passengerId)) {
        return CancelTripResponse(
            status: MatchingState.REJECTED,
            message: "Please contact the Admins.");
      }
      //if end location is reached
      //If there is a trip after the end ether bridge or just delete driver trip
      //if stop is used by other passenger
      //If there is no trip after the end just delete trip
      if (currendDriverTrip.endAdress == trip.destinationDisplayName) {
        DriverTrip? nextDriverTripTrip =
            await nextDriverTrip(currendDriverTrip);
        if (nextDriverTripTrip != null) {
          if (nextDriverTripTrip.availableSeats ==
                  (currendDriverTrip.availableSeats! - 1) &&
              currendDriverTrip.passengers!.length != 1) {
            await removeTrip(nextDriverTripTrip);
            await removeTrip(currendDriverTrip);
            await createDriverTrip(currendDriverTrip, nextDriverTripTrip);
          } else if (nextDriverTripTrip.availableSeats ==
              (currendDriverTrip.availableSeats! - 1)) {
            removeTrip(currendDriverTrip);
          } else {
            removePassengerFromTrip(currendDriverTrip, trip);
          }
        } else {
          removeTrip(currendDriverTrip);
        }

        break;
      }
      currendDriverTrip = await nextDriverTrip(currendDriverTrip);
    }
    await supabase
        .from('trip')
        .update({'cancelled': true}).match({'id': trip.tripId});
    trip.cancelled = true;
    return CancelTripResponse(status: MatchingState.SUCCESS);
  }

  ///gets the next connected Trip from the database
  Future<DriverTrip?> nextDriverTrip(DriverTrip driverTrip) async {
    List res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', driverTrip.driverId)
        .eq('start_time', driverTrip.endTime)
        .limit(1) as List;
    if (res.isEmpty) {
      return null;
    }
    return DriverTrip.parseDriverOnDuty(res[0] as Map<String, dynamic>);
  }

  //removes user from passengerList and adds one to remaining seats
  Future<void> removePassengerFromTrip(
      DriverTrip driverTrip, PassengerTrip trip) async {
    driverTrip.passengers!.remove(trip.passengerId);
    await supabase.from('driver_trips').update({
      'passengers': jsonEncode(driverTrip.passengers),
      'remaining_seats': driverTrip.availableSeats! + 1
    }).eq('id', driverTrip.driverTripId);
  }

  ///creates new Trip with start from driverTripFrom and end from driverTripTo
  Future<void> createDriverTrip(
      DriverTrip driverTripFrom, DriverTrip driverTripTo) async {
    final DriverTrip newDriverTrip = DriverTrip();

    newDriverTrip.driverId = driverTripFrom.driverId;
    newDriverTrip.startTime = driverTripFrom.startTime;
    newDriverTrip.endTime = driverTripTo.endTime;
    newDriverTrip.startLocation = driverTripFrom.startLocation;
    newDriverTrip.endLocation = driverTripTo.endLocation;
    newDriverTrip.passengers = driverTripFrom.passengers;
    newDriverTrip.availableSeats = driverTripFrom.availableSeats;

    newDriverTrip.duration = (await getRouteResource(
      driverTripFrom.startLocation!,
      driverTripTo.endLocation!,
    ))
        .content
        .time;

    newDriverTrip.startAdress = driverTripFrom.startAdress;
    newDriverTrip.endAdress = driverTripTo.endAdress;
    await newDriverTrip.saveToSupabase();
  }

  ///removes Trip from Database
  Future<void> removeTrip(DriverTrip driverTrip) async {
    await supabase
        .from('driver_trips')
        .delete()
        .eq('driver_id', driverTrip.driverId)
        .eq('start_time', driverTrip.startTime);
  }
}
