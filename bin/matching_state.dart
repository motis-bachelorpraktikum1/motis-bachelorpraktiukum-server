enum MatchingState {
  SUCCESS, //Matched succesfully
  FAILED, //Trip could not be matched
  REJECTED, //Matching did throw an error
  ERROR //Bad request
}
