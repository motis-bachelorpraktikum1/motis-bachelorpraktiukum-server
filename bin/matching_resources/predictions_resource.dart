import 'dart:convert';
import 'package:http/http.dart';

Future<PredictionsResource> getResource(String adress) async {
  final Request request =
      Request('POST', Uri.parse('https://europe.motis-project.de/'));

  request.body = jsonEncode({
    'destination': {"type": "Module", "target": "/address"},
    'content_type': "AddressRequest",
    'content': {"input": adress}
  });

  request.headers['Content-Type'] = 'application/json';
  request.headers['Accept'] = '*/*';
  request.headers['Accept-Encoding'] = 'gzip, deflate, br';

  final responseStream = await request.send();

  final String responseString = await responseStream.stream.bytesToString();

  if (responseStream.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON
    final response = jsonDecode(responseString);
    return PredictionsResource.fromJson(response as Map<String, dynamic>);
  } else {
    throw Exception('Failed to create Predicitions.');
  }
}

class PredictionsResource {
  final ContentModel content;

  const PredictionsResource({required this.content});

  factory PredictionsResource.fromJson(Map<String, dynamic> json) {
    final contentJson = json['content'] as Map<String, dynamic>;
    return PredictionsResource(content: ContentModel.fromJson(contentJson));
  }
}

class ContentModel {
  final List<GuessesModel> guesses;

  const ContentModel({required this.guesses});

  factory ContentModel.fromJson(Map<String, dynamic> json) {
    return ContentModel(
      guesses: (json['guesses'] as List<dynamic>)
          .map((e) => GuessesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }
}

class GuessesModel {
  final String name;
  final String type;
  final PositionModel pos;
  final List<RegionModel> regions;

  const GuessesModel({
    required this.name,
    required this.type,
    required this.pos,
    required this.regions,
  });

  factory GuessesModel.fromJson(Map<String, dynamic> json) {
    return GuessesModel(
      name: json['name'] as String,
      type: json['type'] as String,
      pos: PositionModel.fromJson(json['pos'] as Map<String, dynamic>),
      regions: (json['regions'] as List<dynamic>)
          .map((e) => RegionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }
}

class PositionModel {
  num latitude;
  num lng;

  PositionModel({required this.latitude, required this.lng});

  @override
  String toString() => '$latitude, $lng';

  factory PositionModel.fromJson(Map<String, dynamic> json) {
    return PositionModel(
      latitude: double.parse(json['lat'].toString()),
      lng: double.parse(json['lng'].toString()),
    );
  }

  Map<String, dynamic> toJson() => {
        'lat': latitude,
        'lng': lng,
      };
}

class RegionModel {
  final String name;
  final int adminLevel; //2 = Land, 4 = Bundesland, 8 = Stadt

  const RegionModel({required this.name, required this.adminLevel});

  factory RegionModel.fromJson(Map<String, dynamic> json) {
    return RegionModel(
      name: json['name'] as String,
      adminLevel: json['admin_level'] as int,
    );
  }
}

class Suggestions {
  String streetName;
  String cityName;

  Suggestions({
    required this.streetName,
    required this.cityName,
  });

  String parseString() {
    return "$streetName, $cityName";
  }
}
