// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'osrm_route_resource.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Content _$ContentFromJson(Map<String, dynamic> json) => Content(
      time: json['time'] as num,
      distance: json['distance'] as num,
      polyline:
          PolyLineModel.fromJson(json['polyline'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContentToJson(Content instance) => <String, dynamic>{
      'time': instance.time,
      'distance': instance.distance,
      'polyline': instance.polyline.toJson(),
    };

PolyLineModel _$PolyLineModelFromJson(Map<String, dynamic> json) =>
    PolyLineModel(
      coordinates: (json['coordinates'] as List<dynamic>)
          .map((e) => (e as num).toDouble())
          .toList(),
    );

Map<String, dynamic> _$PolyLineModelToJson(PolyLineModel instance) =>
    <String, dynamic>{
      'coordinates': instance.coordinates,
    };

TripInformation _$TripInformationFromJson(Map<String, dynamic> json) =>
    TripInformation(
      content: Content.fromJson(json['content'] as Map<String, dynamic>),
      passengerId: json['passengerId'] as String,
      startAdress: json['startAdress'] as String,
      destinationAdress: json['destinationAdress'] as String,
      startPos:
          PositionModel.fromJson(json['startPos'] as Map<String, dynamic>),
      destPos: PositionModel.fromJson(json['destPos'] as Map<String, dynamic>),
      pickedDate: DateTime.parse(json['pickedDate'] as String),
      isDeparturePicked: json['isDeparturePicked'] as bool,
    );

Map<String, dynamic> _$TripInformationToJson(TripInformation instance) =>
    <String, dynamic>{
      'content': instance.content.toJson(),
      'passengerId': instance.passengerId,
      'startAdress': instance.startAdress,
      'destinationAdress': instance.destinationAdress,
      'startPos': instance.startPos.toJson(),
      'destPos': instance.destPos.toJson(),
      'pickedDate': instance.pickedDate.toIso8601String(),
      'isDeparturePicked': instance.isDeparturePicked,
    };
