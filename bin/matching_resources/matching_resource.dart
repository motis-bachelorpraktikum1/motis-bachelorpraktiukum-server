import 'package:latlong2/latlong.dart';

//Class to model trips from Trip Table
class TripModel {
  String? driverID;
  String? passengerID;
  DateTime? scheduledOn;
  String? startPointCoordinateString;
  String? destPointCoordinateString;
  LatLng? destinationCoordinate;
  LatLng? startPointCoordinate;
  String? startDisplayName;
  String? destinationDisplayName;
  num? duration;

  // ignore: unused_element
  TripModel._toTrip(Map<String, dynamic> result) {
    passengerID = result['passenger'] as String;
    scheduledOn = result['scheduled_on'] as DateTime;
    destinationCoordinate =
        _resolveCoordinateString(result['destination_coordinate'] as String);
    startPointCoordinate =
        _resolveCoordinateString(result['start_point_coordinate'] as String);
    startDisplayName = result['start_display_name'] as String;
    destinationDisplayName = result['destination_display_name'] as String;
    duration = result['duration'] as num;
  }

  LatLng _resolveCoordinateString(String coordinateString) {
    final int idx = coordinateString.indexOf(", ");
    final List<double> parts = [
      double.parse(coordinateString.substring(0, idx).trim()),
      double.parse(coordinateString.substring(idx + 1).trim()),
    ];
    final LatLng coordinates = LatLng(parts[0], parts[1]);
    return coordinates;
  }

  // ignore: prefer_constructors_over_static_methods
  static Future<TripModel> toTrip(Map<String, dynamic> result) async {
    final trip = TripModel._toTrip(result);
    return trip;
  }
}
