// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'matching_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MatchingResponse _$MatchingResponseFromJson(Map<String, dynamic> json) =>
    MatchingResponse(
      status: $enumDecode(_$MatchingStateEnumMap, json['status']),
      driverId: json['driverId'] as String?,
      departureInfo: json['departureInfo'] as String?,
      arrivalInfo: json['arrivalInfo'] as String?,
    );

Map<String, dynamic> _$MatchingResponseToJson(MatchingResponse instance) =>
    <String, dynamic>{
      'status': _$MatchingStateEnumMap[instance.status]!,
      'driverId': instance.driverId,
      'departureInfo': instance.departureInfo,
      'arrivalInfo': instance.arrivalInfo,
    };

const _$MatchingStateEnumMap = {
  MatchingState.SUCCESS: 'SUCCESS',
  MatchingState.FAILED: 'FAILED',
  MatchingState.REJECTED: 'REJECTED',
  MatchingState.ERROR: 'ERROR',
};
