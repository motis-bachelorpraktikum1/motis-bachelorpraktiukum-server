import 'package:intl/intl.dart';
import 'package:supabase/supabase.dart';
import '../supabase_resources/driver.dart';
import '../supabase_resources/driver_trips.dart';
import 'osrm_route_resource.dart';
import '../supabase_resources/constants.dart';

String formatDate(DateTime dateTime) {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  return formatter.format(dateTime);
}

Future<List<Driver>> findSuitableDrivers(
    TripInformation tripInformation) async {
  final DateTime scheduleDate = tripInformation.pickedDate;

  final data = await supabase.rpc(
    'get_possible_drivers',
    params: {
      'scheduled_date': scheduleDate.toIso8601String(),
      'lat': tripInformation.startPos.latitude,
      'long': tripInformation.startPos.lng,
    },
  ) as List;

  List<Driver> possibleDrivers = data
      .map((e) => Driver.parseDriverModel(e as Map))
      .cast<Driver>()
      .toList();

  return possibleDrivers;
}

Future<DriverTrip?> selectTripByTime(
  DateTime pickedTime,
  String driverId,
  // ignore: avoid_positional_boolean_parameters
  bool isDeparturePicked,
) async {
  List res;
  if (isDeparturePicked) {
    res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', driverId)
        .lte('start_time', pickedTime)
        .gt('end_time', pickedTime)
        .order('start_time', ascending: true) as List;
  } else {
    res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', driverId)
        .lt('start_time', pickedTime)
        .gte('end_time', pickedTime)
        .order('start_time', ascending: true) as List;
  }
  if (res.isEmpty) return null;
  if (res.length > 1) {
    throw Error();
  }
  return DriverTrip.parseDriverOnDuty(res[0] as Map);
}

Future<DriverTrip?> selectPreviousTrip(
  DateTime pickedTime,
  String driverId,
  bool isDeparturePicked,
) async {
  List res;
  if (isDeparturePicked) {
    res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', driverId)
        .lte('end_time', pickedTime)
        .order('end_time', ascending: false)
        .limit(1) as List;
  } else {
    res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', driverId)
        .gte('start_time', pickedTime)
        .order('start_time', ascending: true)
        .limit(1) as List;
  }

  if (res.isEmpty) return null;
  return DriverTrip.parseDriverOnDuty(res[0] as Map);
}

Future<DriverTrip?> selectSubsequentTrip(
  DateTime pickedTime,
  String driverId,
  bool isDeparturePicked,
) async {
  List res;
  if (isDeparturePicked) {
    res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', driverId)
        .gte('start_time', pickedTime)
        .order('start_time', ascending: true)
        .limit(1) as List;
  } else {
    res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', driverId)
        .lte('end_time', pickedTime)
        .order('end_time', ascending: false)
        .limit(1) as List;
  }
  if (res.isEmpty) return null;
  return DriverTrip.parseDriverOnDuty(res[0] as Map);
}

Future<DateTime> getLastForStop(String id, DateTime pickedTime,
    DateTime lastTime, bool isDeparturePicked) async {
  String dockName = isDeparturePicked ? 'end_time' : 'start_time';
  final res = await supabase
      .from('driver_schedule')
      .select(dockName)
      .eq('driver_id', id)
      .lte('start_time', pickedTime)
      .gte('end_time', pickedTime)
      .limit(1) as List;
  String retTimeString = (res[0] as Map<String, dynamic>)[dockName] as String;
  return DateTime.parse(retTimeString);
}

Future<int> getAvailableSeats(String id) async {
  final res = await supabase
      .from('vehicle')
      .select('number_of_seats')
      .eq('driver_id', id)
      .single() as dynamic;
  return res['number_of_seats'] as int;
}

Future<void> removeTrip(DriverTrip trip) async {
  await supabase.from('driver_trips').delete().eq('id', trip.driverTripId);
}

Future<void> updateTrip(DriverTrip trip, String id) async {
  trip.passengers!.add(id);
  await supabase.from('driver_trips').update({
    'remaining_seats': (trip.availableSeats! - 1),
    'passenger': trip.passengers,
  }).match({'id': trip.driverTripId});
}
