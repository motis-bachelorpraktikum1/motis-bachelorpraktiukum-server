import 'package:json_annotation/json_annotation.dart';
import '../matching_state.dart';

part 'matching_response.g.dart';

@JsonSerializable(explicitToJson: true)
class MatchingResponse {
  MatchingState status;
  String? driverId;
  String? departureInfo;
  String? arrivalInfo;

  MatchingResponse(
      {required this.status,
      this.driverId,
      this.departureInfo,
      this.arrivalInfo});

  factory MatchingResponse.fromJson(Map<String, dynamic> json) =>
      _$MatchingResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MatchingResponseToJson(this);
}
