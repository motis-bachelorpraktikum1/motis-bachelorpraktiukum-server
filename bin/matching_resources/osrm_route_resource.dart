import 'dart:convert';
import 'package:http/http.dart';

import 'package:json_annotation/json_annotation.dart';

import 'predictions_resource.dart';

part 'osrm_route_resource.g.dart';

Future<RouteResponse> getRouteResource(
  PositionModel startPos,
  PositionModel destPos,
) async {
  final Request request =
      Request('POST', Uri.parse('https://europe.motis-project.de/'));

  request.body = jsonEncode({
    'destination': {"target": "/osrm/via", "type": "Module"},
    'content': {
      "profile": "car",
      "waypoints": [
        {"lat": startPos.latitude, "lng": startPos.lng},
        {"lat": destPos.latitude, "lng": destPos.lng}
      ]
    },
    'content_type': "OSRMViaRouteRequest",
  });

  request.headers['Content-Type'] = 'application/json';
  request.headers['Accept'] = '*/*';
  request.headers['Accept-Encoding'] = 'gzip, deflate, br';

  final responseStream = await request.send();

  final String responseString = await responseStream.stream.bytesToString();

  if (responseStream.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON
    final response = jsonDecode(responseString);
    return RouteResponse.fromJson(response as Map<String, dynamic>);
  } else {
    throw Exception('Failed to create Predicitions.');
  }
}

class RouteResponse {
  final Content content;

  const RouteResponse({required this.content});

  factory RouteResponse.fromJson(Map<String, dynamic> json) {
    return RouteResponse(
      content: Content.fromJson(json['content'] as Map<String, dynamic>),
    );
  }
}

@JsonSerializable(explicitToJson: true)
class Content {
  final num time;
  final num distance;
  final PolyLineModel polyline;

  const Content({
    required this.time,
    required this.distance,
    required this.polyline,
  });

  factory Content.fromJson(Map<String, dynamic> json) =>
      _$ContentFromJson(json);

  Map<String, dynamic> toJson() => _$ContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class PolyLineModel {
  final List<double> coordinates;

  const PolyLineModel({
    required this.coordinates,
  });

  factory PolyLineModel.fromJson(Map<String, dynamic> json) =>
      _$PolyLineModelFromJson(json);

  Map<String, dynamic> toJson() => _$PolyLineModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class TripInformation {
  final Content content;
  final String passengerId;
  final String startAdress;
  final String destinationAdress;
  final PositionModel startPos;
  final PositionModel destPos;
  final DateTime pickedDate;
  final bool isDeparturePicked;

  const TripInformation({
    required this.content,
    required this.passengerId,
    required this.startAdress,
    required this.destinationAdress,
    required this.startPos,
    required this.destPos,
    required this.pickedDate,
    required this.isDeparturePicked,
  });

  factory TripInformation.fromJson(Map<String, dynamic> json) =>
      _$TripInformationFromJson(json);

  Map<String, dynamic> toJson() => _$TripInformationToJson(this);
}
