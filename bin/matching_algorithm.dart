import 'package:intl/intl.dart';

import 'matching_resources/driver_matching_util.dart';
import 'matching_resources/matching_response.dart';
import 'matching_resources/osrm_route_resource.dart';
import 'matching_resources/predictions_resource.dart';
import 'matching_state.dart';
import 'supabase_resources/constants.dart';
import 'supabase_resources/driver.dart';
import 'supabase_resources/driver_trips.dart';
import 'supabase_resources/trip.dart';
import 'package:uuid/uuid.dart';

class MatchingAlgorithm {
  final bool _isDeparturePicked;
  final int _drivers = 1;
  final PositionModel _positionTimeIsPickedFor;
  final PositionModel _positionWithNoTime;
  final TripInformation _tripInformation;
  final num _tripDurationOvertimeFactor = 5.0;
  final DateTime pickedDate;
  final String passengerId;

  DateTime _lastTimeForStop;
  String _output = '';
  var _loading = true;

  MatchingAlgorithm(TripInformation tripInformation)
      : _tripInformation = tripInformation,
        _isDeparturePicked = tripInformation.isDeparturePicked,
        _positionTimeIsPickedFor = tripInformation.isDeparturePicked
            ? tripInformation.startPos
            : tripInformation.destPos,
        _positionWithNoTime = tripInformation.isDeparturePicked
            ? tripInformation.destPos
            : tripInformation.startPos,
        _lastTimeForStop = DateTime.now(),
        pickedDate = tripInformation.pickedDate,
        passengerId = tripInformation.passengerId;

  ///manages the Methods from finding the optimal route to generating/updating the
  ///related Trips and returning if a route was found
  Future<MatchingResponse> matchTrip() async {
    //proofs if there is a need for a trip
    if (_tripInformation.content.time == 0)
      return MatchingResponse(status: MatchingState.REJECTED);
    //find the optimal route for the given trip
    final Map<String, dynamic> route = await findRoute();
    //Proof if there is a valid route else routeTrip is empty
    if ((route['routeTrips'] as List).isEmpty) {
      return MatchingResponse(status: MatchingState.FAILED);
    }
    //generate driver_trips and receive final time for not picked location
    DateTime otherTime = await splitRoute(route);
    //generate trip for passenger
    await createPassengerTrip(route, otherTime);
    //Set Times and Locations for booked Trip
    final String departureInfo = _isDeparturePicked
        ? '${DateFormat('dd.MM.yyyy - kk:mm').format(pickedDate)} in ${_tripInformation.startAdress}'
        : '${DateFormat('dd.MM.yyyy - kk:mm').format(otherTime)} in ${_tripInformation.startAdress}';

    final String arrivalInfo = _isDeparturePicked
        ? '${DateFormat('dd.MM.yyyy - kk:mm').format(otherTime)} in ${_tripInformation.destinationAdress}'
        : '${DateFormat('dd.MM.yyyy - kk:mm').format(pickedDate)} in ${_tripInformation.destinationAdress}';
    return MatchingResponse(
        status: MatchingState.SUCCESS,
        driverId: route['id'],
        departureInfo: departureInfo,
        arrivalInfo: arrivalInfo);
  }

  ///Generates an entry into the Trip Table to give the Passenger an Trip
  Future<void> createPassengerTrip(
      Map<String, dynamic> route, DateTime notPickedTime) async {
    Trip trip = Trip();
    trip.tripId = const Uuid().v4();
    trip.requestedAt = DateTime.now();
    trip.driverId = route['id'] as String;
    trip.passengerId = passengerId;
    trip.cancelled = false;
    trip.duration = pickedDate.difference(notPickedTime).inSeconds.abs();
    trip.scheduledOn = _isDeparturePicked ? pickedDate : notPickedTime;
    trip.startLocation = _tripInformation.startPos.toString();
    trip.endLocation = _tripInformation.destPos.toString();
    trip.startPointDisplayName = _tripInformation.startAdress;
    trip.destinationDisplayName = _tripInformation.destinationAdress;
    trip.isDeparturePicked = _isDeparturePicked;
    trip.saveToSupabase();
  }

  ///Finds the optimal route for the given Trip an returns it an an Map with
  ///'id' as the Id of the selected Driver 'routeTrips' as the selection of
  ///preexisting trips ans null for gabs between trips and 'routeLength' as length
  ///of the route
  Future<Map<String, dynamic>> findRoute() async {
    //get suitable drivers
    final List<Driver> possibleDrivers =
        await findSuitableDrivers(_tripInformation);
    DateTime lastTimeForPassenger = calcTimeWithSeconds(
      pickedDate,
      (_tripInformation.content.time * _tripDurationOvertimeFactor).toInt(),
    );
    //initiate Map to return later
    Map<String, dynamic> route = {
      'routeTrips': [],
      'routeLength': 0x8000000000000000 /*64-bit*/,
    };
    //iterate over Drivers
    for (final driver in possibleDrivers) {
      final DriverTrip? trip = await selectTripByTime(
          pickedDate, driver.userId!, _isDeparturePicked);
      DateTime sheduleEnd = await getLastForStop(
        driver.userId!,
        pickedDate,
        lastTimeForPassenger,
        _isDeparturePicked,
      );
      //set last time this Driver should have a Stop for this trip
      _lastTimeForStop = isAfter(sheduleEnd, lastTimeForPassenger)
          ? lastTimeForPassenger
          : sheduleEnd;
      bool isFitInTrip;
      if (trip != null) {
        /*We can ignore Length if both departure and arrival fit into the same
        trip, because then we drive the direct route, which is always the shortest*/
        if (trip.availableSeats! <= 0) continue;
        isFitInTrip = await doesStopMatchInTrip(trip, _positionTimeIsPickedFor);
        if (!isFitInTrip) {
          continue;
        }
        isFitInTrip = await doStopsMatchInTrip(
          trip,
          _positionTimeIsPickedFor,
          _positionWithNoTime,
        );
        if (!isFitInTrip) {
          route = await recurseOverTrips(
              route, trip, null, [trip], 0, driver.userId!);
        } else {
          route['routeTrips'] = [trip];
          route['id'] = driver.userId;
          continue;
        }
      } else {
        //get his previous trip and subsequent trip
        final DriverTrip? nextTrip = await selectSubsequentTrip(
            pickedDate, driver.userId!, _isDeparturePicked);
        final DriverTrip? beforeTrip = await selectPreviousTrip(
            pickedDate, driver.userId!, _isDeparturePicked);
        if (nextTrip == null && beforeTrip == null) {
          final int fastestArrival = await routeDuration(
              _positionTimeIsPickedFor, _positionWithNoTime);
          final DateTime arrival =
              calcTimeWithSeconds(pickedDate, fastestArrival);
          if (isAfter(pickedDate, _lastTimeForStop) ||
              isAfter(arrival, _lastTimeForStop)) {
            continue;
          }
          route['routeTrips'] = [null];
          route['id'] = driver.userId;
          break;
        }
        if (beforeTrip != null) {
          PositionModel dockPoint = _isDeparturePicked
              ? beforeTrip.startLocation!
              : beforeTrip.endLocation!;
          DateTime dockTime =
              _isDeparturePicked ? beforeTrip.startTime! : beforeTrip.endTime!;
          final int routeTimePick = await routeDuration(
            dockPoint,
            _positionTimeIsPickedFor,
          );
          DateTime subToPicked = calcTimeWithSeconds(dockTime, routeTimePick);
          if (isAfter(subToPicked, pickedDate)) {
            continue;
          }
        }
        if (nextTrip != null) {
          PositionModel endDock = _isDeparturePicked
              ? nextTrip.startLocation!
              : nextTrip.endLocation!;
          DateTime endDockTime =
              _isDeparturePicked ? nextTrip.startTime! : nextTrip.endTime!;

          final int pickedToNotPicked = await routeDuration(
              _positionTimeIsPickedFor, _positionWithNoTime);
          DateTime pickedToNotPickedArrival =
              calcTimeWithSeconds(pickedDate, pickedToNotPicked);

          if (isAfter(pickedToNotPickedArrival, endDockTime)) {
            final int distance =
                await routeDistance(_positionTimeIsPickedFor, endDock);
            route = await recurseOverTrips(
                route, beforeTrip, null, [null], distance, driver.userId!);
          }

          final int notPickedToEndDock =
              await routeDuration(_positionWithNoTime, endDock);
          DateTime arrivalTime = calcTimeWithSeconds(
            pickedDate,
            pickedToNotPicked + notPickedToEndDock,
          );
          if (isAfter(arrivalTime, endDockTime)) {
            int distance = await routeDistance(
              _positionTimeIsPickedFor,
              endDock,
            );
            route = await recurseOverTrips(
              route,
              beforeTrip,
              null,
              [null],
              distance,
              driver.userId!,
            );
          } else {
            route['routeTrips'] = [null];
            route['id'] = driver.userId;
          }
        } else {
          route['routeTrips'] = [trip];
          route['id'] = driver.userId;
        }
      }
    }
    return route;
  }

  ///Checks if it is possible to arrive at destPos on arriveTime when driving
  ///from startPos at startTime
  Future<bool> onTime(DateTime startTime, DateTime arriveTime,
      PositionModel startPos, PositionModel destPos) async {
    int tripRoute = await routeDuration(startPos, destPos);
    DateTime getArrivalTime = calcTimeWithSeconds(
      startTime,
      tripRoute,
    );
    return !getArrivalTime.isAfter(arriveTime);
  }

  ///takes the given route and ether creates a new trip when there was no before
  ///or splits a given Trip and adds a passenger for every given object in routeTrips.
  ///
  ///returns the endTime of the not picked location
  Future<DateTime> splitRoute(Map<String, dynamic> route) async {
    //not short vars
    //trips
    DriverTrip? trip = route['routeTrips'][0] as DriverTrip?;
    List<DriverTrip?> routeTrips = route['routeTrips'] as List<DriverTrip?>;
    //booking (passagier fahrt nur dann erstellen wenn fahrer gefunden)
    if (routeTrips.length == 1 && trip == null) {
      int duration = await routeDuration(
          _tripInformation.startPos, _tripInformation.destPos);
      DateTime destTime = calcTimeWithSeconds(
        pickedDate,
        duration + 600,
      );
      DriverTrip? nextTrip =
          await tripNearTime(pickedDate, route['id'] as String);
      destTime =
          isAfter(destTime, _lastTimeForStop) ? _lastTimeForStop : destTime;
      if (nextTrip != null) {
        DateTime endDockTime =
            _isDeparturePicked ? nextTrip.startTime! : nextTrip.endTime!;
        destTime = isAfter(destTime, endDockTime) ? endDockTime : destTime;
      }

      DateTime firstStopTime = _isDeparturePicked ? pickedDate : destTime;
      DateTime secoundStopTime = _isDeparturePicked ? destTime : pickedDate;

      await createTrip(
        route['id'] as String,
        _tripInformation.startPos,
        firstStopTime,
        _tripInformation.startAdress,
        _tripInformation.destPos,
        secoundStopTime,
        _tripInformation.destinationAdress,
      );

      return destTime;
    } else if (routeTrips[0] == null) {
      int duration = await routeDuration(
        _tripInformation.startPos,
        _tripInformation.destPos,
      );
      DateTime destTime = calcTimeWithSeconds(
        pickedDate,
        duration,
      );
      String pickedAdress = _isDeparturePicked
          ? _tripInformation.startAdress
          : _tripInformation.destinationAdress;
      DriverTrip? nextTrip = routeTrips[1];
      if (_isDeparturePicked) {
        await createTrip(
          nextTrip!.driverId!,
          _positionTimeIsPickedFor,
          pickedDate,
          pickedAdress,
          nextTrip.startLocation!,
          nextTrip.startTime!,
          nextTrip.startAdress!,
        );
      } else {
        await createTrip(
          nextTrip!.driverId!,
          nextTrip.endLocation!,
          nextTrip.endTime!,
          nextTrip.endAdress!,
          _positionTimeIsPickedFor,
          pickedDate,
          pickedAdress,
        );
      }
      return iterateOverNextTrips(routeTrips, route['id'] as String);
    } else if (routeTrips.length == 1) {
      await removeTrip(trip!);
      int durationTime =
          (await routeDuration(_positionTimeIsPickedFor, _positionWithNoTime));
      DateTime endTime = calcTimeWithSeconds(
        pickedDate,
        durationTime,
      );
      PositionModel firstStopPos = _tripInformation.startPos;
      PositionModel secondStopPos = _tripInformation.destPos;
      DateTime firstStopTime = _isDeparturePicked ? pickedDate : endTime;
      DateTime secondStopTime = _isDeparturePicked ? endTime : pickedDate;
      String firstStopAdress = _tripInformation.startAdress;
      String secondStopAdress = _tripInformation.destinationAdress;
      if (trip.startAdress == _tripInformation.startAdress &&
          !_isDeparturePicked) {
        firstStopPos = trip.startLocation!;
        firstStopTime = trip.startTime!;
        firstStopAdress = trip.startAdress!;
      } else if (trip.startAdress != _tripInformation.startAdress) {
        await createTrip(
          trip.driverId!,
          trip.startLocation!,
          trip.startTime!,
          trip.startAdress!,
          firstStopPos,
          firstStopTime,
          firstStopAdress,
          trip.availableSeats,
          trip.passengers,
        );
      }
      if (trip.endAdress == _tripInformation.destinationAdress &&
          _isDeparturePicked) {
        secondStopPos = trip.endLocation!;
        secondStopTime = trip.endTime!;
        secondStopAdress = trip.endAdress!;
      } else if (trip.endAdress != _tripInformation.destinationAdress) {
        await createTrip(
          trip.driverId!,
          secondStopPos,
          secondStopTime,
          secondStopAdress,
          trip.endLocation!,
          trip.endTime!,
          trip.endAdress!,
          trip.availableSeats,
          trip.passengers,
        );
      }
      await createTrip(
        trip.driverId!,
        firstStopPos,
        firstStopTime,
        firstStopAdress,
        secondStopPos,
        secondStopTime,
        secondStopAdress,
        trip.availableSeats! - 1,
        trip.passengers,
      );
      return endTime;
    } else {
      await removeTrip(trip!);
      int seatsToPickedLocation =
          _isDeparturePicked ? trip.availableSeats! : trip.availableSeats! - 1;
      int seatsFromPickedLocation =
          _isDeparturePicked ? trip.availableSeats! - 1 : trip.availableSeats!;
      PositionModel stopPos = _isDeparturePicked
          ? _tripInformation.startPos
          : _tripInformation.destPos;
      DateTime stopTime = pickedDate;
      String stopAdress = _isDeparturePicked
          ? _tripInformation.startAdress
          : _tripInformation.destinationAdress;
      /*Es könnte sein das er einen Sitz zuwenig Bucht, wenn er zum Beispiel 
      am Selben Ort Startet.
      Aber _isDeparturePicked == false ist. Das sollte nicht passieren aufgrund 
      wie Trips zugeteilt werden in findTripInTime aber man weiß ja nie*/
      if (!trip.startTime!.isAtSameMomentAs(pickedDate)) {
        await createTrip(
            trip.driverId!,
            trip.startLocation!,
            trip.startTime!,
            trip.startAdress!,
            stopPos,
            stopTime,
            stopAdress,
            seatsToPickedLocation,
            trip.passengers);
      }
      if (!pickedDate.isAtSameMomentAs(trip.endTime!)) {
        await createTrip(
          trip.driverId!,
          stopPos,
          stopTime,
          stopAdress,
          trip.endLocation!,
          trip.endTime!,
          trip.endAdress!,
          seatsFromPickedLocation,
          trip.passengers,
        );
      }
      return await iterateOverNextTrips(routeTrips, route['id'] as String);
    }
  }

  ///Goes over Elements of routeTrips till it reatches the end and creates or
  ///updates trips
  ///
  ///returns the endTime of the not picked location
  Future<DateTime> iterateOverNextTrips(
      List<DriverTrip?> routeTrips, String driverId) async {
    for (var i = 1; i < routeTrips.length; i++) {
      if ((routeTrips.length - 1) == i && routeTrips[i] == null) {
        DriverTrip beforTrip = routeTrips[i - 1] as DriverTrip;
        PositionModel startDockPos = _isDeparturePicked
            ? beforTrip.endLocation!
            : beforTrip.startLocation!;
        DateTime startDockTime =
            _isDeparturePicked ? beforTrip.endTime! : beforTrip.startTime!;
        String startDockAdress =
            _isDeparturePicked ? beforTrip.endAdress! : beforTrip.startAdress!;
        final int durationTime = await routeDuration(
          startDockPos,
          _positionWithNoTime,
        );
        DateTime endTime = calcTimeWithSeconds(
          startDockTime,
          durationTime + 600,
        );
        endTime =
            isAfter(endTime, _lastTimeForStop) ? _lastTimeForStop : endTime;
        DriverTrip? nextTrip = await tripNearTime(pickedDate, driverId);
        DateTime endDockTime =
            _isDeparturePicked ? nextTrip!.startTime! : nextTrip!.endTime!;
        endTime = isAfter(endTime, endDockTime) ? endDockTime : endTime;
        PositionModel fromPos =
            _isDeparturePicked ? startDockPos : _positionWithNoTime;
        PositionModel toPos =
            _isDeparturePicked ? _positionWithNoTime : startDockPos;
        DateTime fromTime = _isDeparturePicked ? startDockTime : endTime;
        DateTime toTime = _isDeparturePicked ? endTime : startDockTime;
        String noTimePickedAdress = _isDeparturePicked
            ? _tripInformation.destinationAdress
            : _tripInformation.startAdress;
        String fromAdress =
            _isDeparturePicked ? startDockAdress : noTimePickedAdress;
        String toAdress =
            _isDeparturePicked ? noTimePickedAdress : startDockAdress;
        await createTrip(
          beforTrip.driverId!,
          fromPos,
          fromTime,
          fromAdress,
          toPos,
          toTime,
          toAdress,
        );
        return endTime;
      } else if (routeTrips.length - 1 == i) {
        DriverTrip currentTrip = routeTrips[i] as DriverTrip;
        removeTrip(currentTrip);

        PositionModel startDockPos = _isDeparturePicked
            ? currentTrip.startLocation!
            : currentTrip.endLocation!;
        DateTime startDockTime =
            _isDeparturePicked ? currentTrip.startTime! : currentTrip.endTime!;
        String startDockAdress = _isDeparturePicked
            ? currentTrip.startAdress!
            : currentTrip.endAdress!;

        PositionModel endDockPos = _isDeparturePicked
            ? currentTrip.endLocation!
            : currentTrip.startLocation!;
        DateTime endDockTime =
            _isDeparturePicked ? currentTrip.endTime! : currentTrip.startTime!;
        String endDockAdress = _isDeparturePicked
            ? currentTrip.endAdress!
            : currentTrip.startAdress!;

        int durationTime = await routeDuration(
          startDockPos,
          _positionWithNoTime,
        );
        DateTime endTime = calcTimeWithSeconds(
          startDockTime,
          durationTime,
        );
        String endAdress = _isDeparturePicked
            ? _tripInformation.destinationAdress
            : _tripInformation.startAdress;
        int seatsTo = _isDeparturePicked
            ? currentTrip.availableSeats! - 1
            : currentTrip.availableSeats!;
        int seatsFrom = _isDeparturePicked
            ? currentTrip.availableSeats!
            : currentTrip.availableSeats! - 1;
        String noTimePickedAdress = _isDeparturePicked
            ? _tripInformation.destinationAdress
            : _tripInformation.startAdress;
        if (noTimePickedAdress != currentTrip.startAdress) {
          await createTrip(
            currentTrip.driverId!,
            currentTrip.startLocation!,
            currentTrip.startTime!,
            currentTrip.startAdress!,
            _positionWithNoTime,
            endTime,
            noTimePickedAdress,
            seatsTo,
            currentTrip.passengers,
          );
        } else if (noTimePickedAdress == currentTrip.startAdress &&
            !_isDeparturePicked) {
          await createTrip(
            currentTrip.driverId!,
            currentTrip.startLocation!,
            currentTrip.startTime!,
            currentTrip.startAdress!,
            _positionWithNoTime,
            currentTrip.endTime!,
            noTimePickedAdress,
            seatsTo,
            currentTrip.passengers,
          );
        }
        if (noTimePickedAdress != currentTrip.endAdress) {
          await createTrip(
              currentTrip.driverId!,
              _positionWithNoTime,
              endTime,
              noTimePickedAdress,
              currentTrip.endLocation!,
              currentTrip.endTime!,
              currentTrip.endAdress!,
              seatsFrom,
              currentTrip.passengers);
        } else if (noTimePickedAdress == currentTrip.endAdress &&
            _isDeparturePicked) {
          await createTrip(
              currentTrip.driverId!,
              _positionWithNoTime,
              currentTrip.startTime!,
              currentTrip.startAdress!,
              currentTrip.endLocation!,
              currentTrip.endTime!,
              currentTrip.endAdress!,
              seatsFrom,
              currentTrip.passengers);
        }
        return endTime;
      } else if (routeTrips[i] == null) {
        DriverTrip nextTrip = routeTrips[i + 1] as DriverTrip;
        DriverTrip beforTrip = routeTrips[i - 1] as DriverTrip;
        PositionModel fromPos =
            _isDeparturePicked ? beforTrip.endLocation! : nextTrip.endLocation!;
        PositionModel toPos = _isDeparturePicked
            ? nextTrip.startLocation!
            : beforTrip.startLocation!;
        DateTime fromTime =
            _isDeparturePicked ? beforTrip.endTime! : nextTrip.endTime!;
        DateTime toTime =
            _isDeparturePicked ? nextTrip.startTime! : beforTrip.startTime!;
        String fromAdress =
            _isDeparturePicked ? beforTrip.endAdress! : nextTrip.endAdress!;
        String toAdress =
            _isDeparturePicked ? beforTrip.startAdress! : nextTrip.startAdress!;
        await createTrip(beforTrip.driverId!, fromPos, fromTime, fromAdress,
            toPos, toTime, toAdress);
      } else {
        await updateTrip(routeTrips[i] as DriverTrip, passengerId);
      }
    }
    throw Exception("An Error occured. Please contact the Admins.");
  }

  ///Creates a trip and saves it onto the Database
  ///
  ///If avalible seats is not given the method gets the seats of the current car
  ///of the selected driver and subtracts 1
  ///
  ///If the passenger is not given a List with the current passenger Id will be
  ///generated
  Future<void> createTrip(
    String id,
    PositionModel startPos,
    DateTime startTime,
    String startAdress,
    PositionModel endPos,
    DateTime endTime,
    String endAdress, [
    int? available,
    List<String>? passenger,
  ]) async {
    if (passenger != null) {
      passenger.add(passengerId);
    }
    DriverTrip newTrip = DriverTrip();
    newTrip.availableSeats = available ?? (await getAvailableSeats(id) - 1);

    newTrip.driverId = id;
    newTrip.duration = await routeDuration(startPos, endPos);
    newTrip.passengers = passenger ?? [passengerId];
    newTrip.startLocation = startPos;
    newTrip.startTime = startTime;
    newTrip.endLocation = endPos;
    newTrip.endTime = endTime;
    newTrip.startAdress = startAdress;
    newTrip.endAdress = endAdress;
    await newTrip.saveToSupabase();
  }

  ///Gets the next trip in the given direction and poofs if the position with no
  ///selected time fits between without breaking any requirements.
  ///
  ///returns the given Map with optimal route
  Future<Map<String, dynamic>> recurseOverTrips(
      Map<String, dynamic> route,
      DriverTrip? tripBefore,
      DriverTrip? trip,
      List<DriverTrip?> currendRoute,
      int km,
      String id) async {
    //What was the last Trip. Auf einen Trip can ein Trip oder nichts folgen aber
    //aber auf nichts muss immer ein Trip folgen ansonsten sind wir fertig.
    //Wenn wir also in die nächste Iteration springen war entweder der letzt Trip
    //vorhanden oder es folgt einer.
    final DriverTrip? nextTrip = trip == null
        ? await tripNearTrip(tripBefore!)
        : await findNextTrip(trip);
    currendRoute.add(trip);

    if (trip == null) {
      if (nextTrip == null) {
        //Wenn ein wir in keinem Trip sind und es auch keinen mehr gibt gib die
        //Liste zurück
        PositionModel firstDockPos = _isDeparturePicked
            ? tripBefore!.startLocation!
            : tripBefore!.endLocation!;
        DateTime firstDockTime =
            _isDeparturePicked ? tripBefore.startTime! : tripBefore.endTime!;
        final int durationArrival =
            await routeDuration(firstDockPos, _positionWithNoTime);
        final int distanceArrival =
            await routeDistance(firstDockPos, _positionWithNoTime);
        DateTime arrivalTime =
            calcTimeWithSeconds(firstDockTime, durationArrival);
        if (isAfter(arrivalTime, _lastTimeForStop)) {
          return route;
        }
        if (route['routeLength'] as int < km) {
          return route;
        } else if (route['routeLength'] as int > km + distanceArrival) {
          route['routeTrips'] = currendRoute;
          route['routeLength'] = km + distanceArrival;
          route['id'] = id;
          return route;
        }
        return route;
      } else {
        //Wenn wir in keinem Trip sind es aber einen Nächsten gibt.
        //Schaue ob wir diesen Stop zwischen die beiden Trips quetschen können
        //Suche nach dem nächsten Trip, welcher existieren muss, da wir in der
        //vorherigen iteration nicht rausgesprungen sind (siehe oben)
        //Anker: MaxLenght, TripVoll(Er nimmt niemanden mit), NichtInArbeitsZeit

        PositionModel startDockPosition = _isDeparturePicked
            ? tripBefore!.endLocation!
            : tripBefore!.startLocation!;
        DateTime startDockTime =
            _isDeparturePicked ? tripBefore.endTime! : tripBefore.startTime!;
        int startDockToNotPicked =
            await routeDuration(startDockPosition, _positionWithNoTime);
        DateTime possibleArrival = calcTimeWithSeconds(
          startDockTime,
          startDockToNotPicked,
        );

        if (isAfter(possibleArrival, _lastTimeForStop)) return route;

        PositionModel endDockPosition = _isDeparturePicked
            ? nextTrip.startLocation!
            : nextTrip.endLocation!;
        DateTime endDockTime =
            _isDeparturePicked ? nextTrip.startTime! : nextTrip.endTime!;
        if (isAfter(possibleArrival, endDockTime)) {
          int beforeToNext =
              await routeDistance(startDockPosition, endDockPosition);
          return recurseOverTrips(
              route, trip, nextTrip, currendRoute, km + beforeToNext, id);
        }
        //Schaue ob, die route passt und ob sie Kürzer ist.
        final int notPickedToNext =
            await routeDuration(_positionWithNoTime, endDockPosition);
        DateTime posArrToEndDock =
            calcTimeWithSeconds(possibleArrival, notPickedToNext);
        if (isAfter(posArrToEndDock, endDockTime)) {
          int beforeToNext =
              await routeDistance(startDockPosition, endDockPosition);
          return await recurseOverTrips(
              route, trip, nextTrip, currendRoute, km + beforeToNext, id);
        } else {
          int beforeToNext =
              await routeDistance(startDockPosition, endDockPosition);
          if (route['routeLength'] as int < km) {
            return route;
          } else if (route['routeLength'] as int > km + beforeToNext) {
            route['routeTrips'] = currendRoute;
            route['routeLength'] = km + beforeToNext;
            route['id'] = id;
            return route;
          }
          return route;
        }
      }
    } else {
      //trip is a trip and nextTrip is null
      //Anker: _last...
      final PositionModel startDockPos =
          _isDeparturePicked ? trip.startLocation! : trip.endLocation!;
      final DateTime startDockTime =
          _isDeparturePicked ? trip.startTime! : trip.endTime!;
      final PositionModel endDockPos =
          _isDeparturePicked ? trip.endLocation! : trip.startLocation!;
      final DateTime endDockTime =
          _isDeparturePicked ? trip.endTime! : trip.startTime!;
      final int startDockToNotPicked =
          await routeDuration(startDockPos, _positionWithNoTime);
      DateTime arrivelTime = calcTimeWithSeconds(
        startDockTime,
        startDockToNotPicked,
      );

      if (trip.availableSeats! <= 0 || isAfter(arrivelTime, _lastTimeForStop))
        return route;

      bool isFit = await doesStopFitBetweenStops(
        startDockPos,
        startDockTime,
        endDockPos,
        endDockTime,
        _positionWithNoTime,
      );
      //Schaue ob, die route passt und ob sie Kürzer ist.
      final int beforeToNext = await routeDistance(startDockPos, endDockPos);
      if (isFit) {
        if (route['routeLength'] as int < km) {
          return route;
        } else if (route['routeLength'] as int > km + beforeToNext) {
          route['routeTrips'] = currendRoute;
          route['routeLength'] = km + beforeToNext;
          route['id'] = id;
          return route;
        }
        return route;
      } else {
        return await recurseOverTrips(
            route, trip, nextTrip, currendRoute, km + beforeToNext, id);
      }
    }
  }

  ///Returns the trip that is connected to current trips in the given direction.
  Future<DriverTrip?> findNextTrip(DriverTrip trip) async {
    String kindOfPosition = _isDeparturePicked ? 'start_time' : 'end_time';
    String atTime = _isDeparturePicked
        ? trip.endTime!.toString()
        : trip.startTime!.toString();
    final res = await supabase
        .from('driver_trips')
        .select()
        .eq('driver_id', trip.driverId)
        .eq(kindOfPosition, atTime)
        .limit(1) as List;

    if (res.isNotEmpty) {
      return DriverTrip.parseDriverOnDuty(res[0] as Map);
    } else {
      return null;
    }
  }

  ///Returns the trip that is scheduled after or before the current trip
  ///depending on the given time (departure or arrival time).
  // ignore: avoid_positional_boolean_parameters
  Future<DriverTrip?> tripNearTrip(DriverTrip trip) async {
    List res;
    if (_isDeparturePicked) {
      res = await supabase
          .from('driver_trips')
          .select()
          .eq('driver_id', trip.driverId)
          .gt('start_time', trip.endTime)
          .order('start_time', ascending: true)
          .limit(1) as List;
    } else {
      res = await supabase
          .from('driver_trips')
          .select()
          .eq('driver_id', trip.driverId)
          .lt('end_time', trip.startTime)
          .order('end_time', ascending: false)
          .limit(1) as List;
    }
    if (res.isNotEmpty) {
      return DriverTrip.parseDriverOnDuty(res[0] as Map);
    } else {
      return null;
    }
  }

  ///Returns Trip that is next in the given direction
  Future<DriverTrip?> tripNearTime(DateTime pickedTime, String driverId) async {
    List res;
    if (_isDeparturePicked) {
      res = await supabase
          .from('driver_trips')
          .select()
          .eq('driver_id', driverId)
          .gt('start_time', pickedTime)
          .order('start_time', ascending: true)
          .limit(1) as List;
    } else {
      res = await supabase
          .from('driver_trips')
          .select()
          .eq('driver_id', driverId)
          .lt('end_time', pickedTime)
          .order('end_time', ascending: false)
          .limit(1) as List;
    }
    if (res.isNotEmpty) {
      return DriverTrip.parseDriverOnDuty(res[0] as Map);
    } else {
      return null;
    }
  }

  ///Checks if a pickup point is inside the time frame of a given trip
  Future<bool> doesStopMatchInTrip(
      DriverTrip trip, PositionModel stopInbetweenLocation) async {
    RouteResponse trip1Route =
        await getRouteResource(trip.startLocation!, stopInbetweenLocation);
    RouteResponse trip2Route =
        await getRouteResource(stopInbetweenLocation, trip.endLocation!);

    num durationWithStop = trip1Route.content.time + trip2Route.content.time;
    return durationWithStop <=
        (trip.startTime!.difference(trip.endTime!).inSeconds).abs();
  }

  ///Checks if pickup and drop point of a trip is inside a given trip
  Future<bool> doStopsMatchInTrip(
    DriverTrip trip,
    PositionModel stop1InbetweenLocation,
    PositionModel stop2InbetweenLocation,
  ) async {
    bool isFit = await onTime(
      trip.startTime!,
      pickedDate,
      trip.startLocation!,
      stop1InbetweenLocation,
    );
    final int trip1Route = await routeDuration(
      stop1InbetweenLocation,
      stop2InbetweenLocation,
    );
    final int trip2Route =
        await routeDuration(stop2InbetweenLocation, trip.endLocation!);
    final DateTime durationWithStop = calcTimeWithSeconds(
      trip.startTime!,
      trip2Route + trip1Route,
    );
    return isAfter(durationWithStop, pickedDate) && isFit;
  }

  ///Retuns a bool indicating if a given stop fits between two given Stops
  ///without breaking any requirements
  Future<bool> doesStopFitBetweenStops(
      PositionModel firstStop,
      DateTime timeFirstStop,
      PositionModel secondStop,
      DateTime timeSecondStop,
      PositionModel stopInbetweenLocation) async {
    final int trip1Route =
        await routeDuration(firstStop, stopInbetweenLocation);
    final int trip2Route =
        await routeDuration(stopInbetweenLocation, secondStop);
    final num durationWithStop = trip1Route + trip2Route;
    final num durationTime =
        timeFirstStop.difference(timeSecondStop).inSeconds.abs();
    return durationWithStop <= durationTime;
  }

  ///returns the estimated Time it takes to Drive from on Location to the other
  Future<int> routeDuration(
      PositionModel firstModel, PositionModel secondModel) async {
    if (_isDeparturePicked) {
      return (await getRouteResource(firstModel, secondModel))
          .content
          .time
          .toInt();
    } else {
      return (await getRouteResource(secondModel, firstModel))
          .content
          .time
          .toInt();
    }
  }

  ///returns the estimated distance it takes to Drive from on Location to the other
  Future<int> routeDistance(
      PositionModel firstModel, PositionModel secondModel) async {
    if (_isDeparturePicked) {
      return (await getRouteResource(firstModel, secondModel))
          .content
          .distance
          .toInt();
    } else {
      return (await getRouteResource(secondModel, firstModel))
          .content
          .distance
          .toInt();
    }
  }

  ///Adds the given Time on the DateTime and Returns the new DateTime acounting
  ///for given direction
  DateTime calcTimeWithSeconds(DateTime timeToCalc, int seconds) {
    if (_isDeparturePicked) {
      return timeToCalc.add(Duration(seconds: seconds));
    }
    return timeToCalc.subtract(Duration(seconds: seconds));
  }

  ///Returns a bool indecating if the given From time is after or equel to
  ///accounting for the given direction
  bool isAfter(DateTime from, DateTime to) {
    if (_isDeparturePicked) {
      return !from.isBefore(to);
    }
    return !from.isAfter(to);
  }
}
