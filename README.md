A server app built using [Shelf](https://pub.dev/packages/shelf),
configured to enable running with [Docker](https://www.docker.com/).

This server handles HTTP GET requests to:
`/`
`/trip/create`
`/trip/cancel`

# Running the Server

## Running with the Dart SDK

You can run the server with the [Dart SDK](https://dart.dev/get-dart)
like this:

```
$ dart run bin/server.dart
Server listening on port 8000
```

And then from a second terminal:

```
$ curl http://localhost:8000'
Motis Server for Driver Matching
```

## Matching and canceling

Use android emulator to communicate with server when running on localhost

'http://10.0.2.2:8000/trip/create'

'http://10.0.2.2:8000/trip/cancel'

## Running with Docker

If you have [Docker Desktop](https://www.docker.com/get-started) installed, you
can build and run with the `docker` command:

```
$ docker build . -t myserver
$ docker run -it -p 8000:8000 myserver
Server listening on port 8000
```

And then from a second terminal:

```
$ curl http://localhost:8000
Hello, World!
```

You should see the logging printed in the first terminal:

```
2023-03-15T17:02:38.650556  0:00:02.530886 POST    [200] /trip/create
2023-03-15T17:03:49.202610  0:00:00.913920 POST    [200] /trip/cancel
```
